# spacestagram

Front End Developer Intern Challenge - Winter 2022

A Curiosity Mars Rover image viewer programmed by Paul Beaudon using the NASA image API.

## Retrieving Images

Use the drop down menu to select the date of the images for viewing and press the green Launch button. If there are images for that date then they will appear. If not there will be a notification saying “No images for this date”. Available dates are from present day back to the first day pictures appeared in the API for the Curiosity Mars Rover (August 06, 2012).

## Navigation

There are two grid modes for viewing the images. <br />

- Three wide
- One wide

The three wide view mode gives the ability to transverse through the set of images quickly. While the one wide view mode gives the ability to see the pictures in a larger format and more information about each. The one wide view mode also allows users to indicate the images that they like by pressing the heart button.
To switch between the two modes press the grid buttons at the top underneath the Launch button or click on any image. Clicking again on the image will switch back to the other grid mode.

## Pagination
Spacestagram will show the first 150 images of the day (if there are that many available) and then the remainder of the pictures can be viewed by pressing the page numbers at the bottom of the page. Each page holds up to 150 images.

## Likes
Pressing the like button will record the users “like” to the database and add to the total of all user likes for that image. If the image was accidentally liked, pressing the like button again will “unlike” the image.
