document.addEventListener('DOMContentLoaded', function() {
    // sign into firebase anonymously to be able to connect to datebase and register votes
    firebase.auth().signInAnonymously().then(() => {
        // anonymous sign in
        dateInitialSetup();
        getImages();
    }).catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.error(`${errorCode}: ${errorMessage}`);
        // continue to get images - likes will not show - but we can still show image info
        dateInitialSetup();
        getImages();
    });
    // would be used for future per person auth
    /*firebase.auth().onAuthStateChanged((user) => {
        if (user) {
            // User is signed in,
            var uid = user.uid;
            console.log(uid);
        } else {
            // User is signed out
            console.log('user signed out');
        }
    });*/
});

const dateInitialSetup = ()=>{
    let todaysArray = todaysDateArray();
    document.getElementById("date-input").setAttribute("max", `${todaysArray[0]}-${todaysArray[1]}-${todaysArray[2]}`);
    document.getElementById("date-input").value = `${todaysArray[0]}-${todaysArray[1]}-${todaysArray[2]}`;
    document.getElementById("date-input").setAttribute("min", '2012-08-06'); // first day of pictures
};

const todaysDateArray = ()=>{
    const today = new Date();
    return [today.getFullYear(),today.getMonth()+1 < 10 ? `0${today.getMonth()+1}` : today.getMonth()+1, today.getDate() < 10 ? `0${today.getDate()}` : today.getDate()];
};

const getImages = ()=>{
    const dateValue = document.getElementById('date-input').value;
    if(dateValue !== ""){
        const queryDate = dateValue.split('-');

        const url = `https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?earth_date=${queryDate[0]}-${queryDate[1]}-${queryDate[2]}&api_key=ETRuPkJnhT6Meff7Feyp2iwZZVJvcPJw2BP3iZLt`
        document.getElementById("warning").innerHTML = 'Loading images...';
        fetch(url).then((res)=>{
            return res.json();
        }).then((data)=>{
            console.log(data);

            let warningString = '';
            if(data.photos.length){
                let photoArray = paginate(data.photos);
                console.log(photoArray);
                displayPhotos(photoArray, 0);
            }else{
                warningString += `<div>No images for this date.</div><div>Please select another date.</div>`;
                document.getElementById('main').innerHTML = '';
                document.getElementById('pages').innerHTML = '';
            }
            document.getElementById("warning").innerHTML = warningString;
            smallGrid();

        }).catch((error)=>{
            console.error(error);
            document.getElementById("warning").innerHTML = `<div>Error loading images</div><div>Please try again.</div>`;
        });
    }else{
        document.getElementById("warning").innerHTML = `<div>No images for this date.</div><div>Please select another date.</div>`;
    }
};
// PAGINATE AND DISPLAY
const paginate = (photos)=>{
    const PAGELIMIT = 150;
    let pagination = [];
    pagination.push([]);
    let paginationCount = 0;
    let photoCount = 0;
    photos.forEach((photo)=>{
        if(photoCount < PAGELIMIT){
            pagination[paginationCount].push(photo);
            photoCount++;
        }else{
            paginationCount++;
            pagination.push([]);
            pagination[paginationCount].push(photo);
            photoCount = 1;
        }
    });
    return pagination;
};

const displayPhotos = (pageArray, pageNumber)=>{
    let mainString = '';
    let warningString = '';
    let pageString = '';

    let gridCount = 0;
    let containerCount = 0;
    let imageCount = 0;

    if(pageArray[pageNumber].length){
        console.log(pageArray[pageNumber]);
        pageArray[pageNumber].forEach((photo)=>{
            if(imageCount === 0){
                mainString += `<div class="grid" id="grid_${gridCount}">`;
            }
            
            mainString += `
                <div class="container" id="container_${containerCount}">
                    <a id="anchor_${containerCount}">
                        <img src="${photo.img_src}" onclick="getInfo(${containerCount})" alt="" />
                    </a>
                </div>
                <div data-id="${photo.id}" data-camera="${photo.camera.full_name}" data-date="${photo.earth_date}" data-sol="${photo.sol}" data-rover="${photo.rover.name}" data-launch="${photo.rover.launch_date}" data-landing="${photo.rover.landing_date}" data-status="${photo.rover.status}" class="img-container" id="img-container_${containerCount}">
                </div>
            `;

            if(imageCount === 2){
                mainString += `</div>`;
                imageCount = 0;
                gridCount++;
            }else{
                imageCount++;
            }
            containerCount++;
        });
    }else{
        warningString += `<div>No images for this date</div><div>Please select another date</div>`;
        document.getElementById('main').innerHTML = '';
        document.getElementById('pages').innerHTML = '';
    }

    if(pageArray.length){
        if(pageArray[0].length){
            pageArray.forEach((page, index)=>{
                pageString += `<div style="cursor:pointer;width:50px;color:${pageNumber === index ? 'white' : 'black'};background-color:${pageNumber === index ? 'black' : 'white'};text-align:center;${index > 0 ? 'border-left:1px solid black' : ''};padding:2px;" id="page_${index}"><i>${index+1}</i></div>`;
            });
        } 
    }
    
    document.getElementById('main').innerHTML = mainString;
    document.getElementById('warning').innerHTML = warningString;
    document.getElementById('pages').innerHTML = pageString;

    if(pageArray.length){
        if(pageArray[0].length){
            pageArray.forEach((page, index)=>{
                let bind = document.getElementById(`page_${index}`);
                bind.onclick = function(){
                    displayPhotos(pageArray, index);
                    scroll(0,0);
                };
            });
        }
    }
    smallGrid();
    
};

const newDateNotification = ()=>{
    document.getElementById('warning').innerHTML = `Press Launch for ${document.getElementById('date-input').value} images`;
    document.getElementById('main').innerHTML = '';
    document.getElementById('pages').innerHTML = '';
};

// GRID LAYOUT
let LARGEGRID = 0;

const smallGrid = ()=>{
    LARGEGRID = 0;
    const grids = document.querySelectorAll("div.grid");
    grids.forEach((grid)=>{
        document.getElementById(grid.id).style.flexDirection = "row";
        document.getElementById(grid.id).style.justifyContent = "space-evenly";
    });

    const containers = document.querySelectorAll("div.container");
    containers.forEach((div)=>{
        document.getElementById(div.id).style.width = '32%';
        document.getElementById(div.id).style.paddingTop = "0";
    });

    const imgInformation = document.querySelectorAll("div.img-container");
    imgInformation.forEach((div)=>{
        document.getElementById(div.id).style.width = '0%';
        document.getElementById(div.id).innerHTML = "";
    });

    document.getElementById("single").className = "";
    document.getElementById("triple").className = "underline";
};

const largeGrid = ()=>{
    LARGEGRID = 1;
    const grids = document.querySelectorAll("div.grid");
    grids.forEach((grid)=>{
        document.getElementById(grid.id).style.flexDirection = "column";
        document.getElementById(grid.id).style.justifyContent = "center";
    });

    const containers = document.querySelectorAll("div.container");
    containers.forEach((div)=>{
        document.getElementById(div.id).style.width = '75%';
        document.getElementById(div.id).style.marginBottom = "0.5vw";
    });

    firebase.firestore().doc(`/likes/${document.getElementById('date-input').value}`).get().then((doc)=>{
        let likeInfo = {};

        if(doc.exists){
            console.log(doc.data());
            const imgIds = Object.keys(doc.data());
            console.log(imgIds);
            likeInfo.data = doc.data();
            likeInfo.imgIds = imgIds;
            return likeInfo;
        }else{
            likeInfo.data = {};
            likeInfo.imgIds = [];
            return likeInfo;
        }
    }).then((likeInfo)=>{
        const imgContainer = document.querySelectorAll("div.img-container");
        imgContainer.forEach((div)=>{
            const likes = likeInfo.imgIds.includes(div.dataset.id) ? likeInfo.data[div.dataset.id] : 0;
            document.getElementById(div.id).style.width = '75%';
            document.getElementById(div.id).innerHTML = `<div class="votebutton" id="photo_${div.dataset.id}" onclick="like('${div.dataset.date}',${div.dataset.id})"><i id="heart_${div.dataset.id}" class="${localStorage.getItem(div.dataset.id) === 'liked' ? 'fa fa-heart' : 'fa fa-heart-o'}" aria-hidden="true"></i><span id="likes_${div.dataset.id}" data-likes="${likes}"> ${likes} </span></div><div class="img-info img-title">Mars Rover: ${div.dataset.rover}</div><div class="img-info img-camera">${div.dataset.camera}</div><div class="img-info img-date">Date: ${div.dataset.date}, Sol: ${div.dataset.sol}</div>`;
        });

    }).catch((error)=>{
        console.log(error);
        // error loading likes - only show image info
        const imgContainer = document.querySelectorAll("div.img-container");
        imgContainer.forEach((div)=>{
            document.getElementById(div.id).style.width = '75%';
            document.getElementById(div.id).innerHTML = `<div class="img-info img-title">Mars Rover: ${div.dataset.rover}</div><div class="img-info img-camera">${div.dataset.camera}</div><div class="img-info img-date">Date: ${div.dataset.date}, Sol: ${div.dataset.sol}</div>`;
        });
    });

    document.getElementById("single").className = "underline";
    document.getElementById("triple").className = "";
};
const getInfo = (position)=>{
    if(LARGEGRID){
        smallGrid();
    }else{
        largeGrid();
    }
    let url = location.href;
    location.href = `#anchor_${position}`;
    history.replaceState(null,null,url);
};
const like = (date, photoId)=>{
    if(localStorage.getItem(photoId) === 'liked'){
        // unlike - save to database and update local storage (future - use user database storage)
        firebase.firestore().doc(`/likes/${date}`).set({
            [`${photoId}`]: firebase.firestore.FieldValue.increment(-1)
        }, { merge: true }).then(()=>{
            localStorage.removeItem(photoId);
            document.getElementById(`heart_${photoId}`).className = 'fa fa-heart-o';
            let likes = parseInt(document.getElementById(`likes_${photoId}`).dataset.likes)-1;
            document.getElementById(`likes_${photoId}`).innerHTML = ` ${likes} `;
            document.getElementById(`likes_${photoId}`).dataset.likes = likes;
        }).catch((error)=>{
            console.log(error);
            // do we remove the button on error saving to database, or leave the button with no change?
        });
    }else{
        // like - save to database and update local storage (future - use user database storage)
        firebase.firestore().doc(`/likes/${date}`).set({
            [`${photoId}`]: firebase.firestore.FieldValue.increment(1)
        }, { merge: true }).then(()=>{
            localStorage.setItem(`${photoId}`, 'liked');
            document.getElementById(`heart_${photoId}`).className = 'fa fa-heart';
            let likes = parseInt(document.getElementById(`likes_${photoId}`).dataset.likes)+1;
            document.getElementById(`likes_${photoId}`).innerHTML = ` ${likes} `;
            document.getElementById(`likes_${photoId}`).dataset.likes = likes;
        }).catch((error)=>{
            console.log(error);
            // do we remove the button on error saving to database, or leave the button with no change?
        });
    }
};